﻿namespace CardRandomizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generateCardButton = new System.Windows.Forms.Button();
            this.outputTexBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // generateCardButton
            // 
            this.generateCardButton.Location = new System.Drawing.Point(12, 233);
            this.generateCardButton.Name = "generateCardButton";
            this.generateCardButton.Size = new System.Drawing.Size(216, 23);
            this.generateCardButton.TabIndex = 0;
            this.generateCardButton.Text = "Generate cards";
            this.generateCardButton.UseVisualStyleBackColor = true;
            this.generateCardButton.Click += new System.EventHandler(this.generateCardButton_Click);
            // 
            // outputTexBox
            // 
            this.outputTexBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.outputTexBox.Location = new System.Drawing.Point(12, 12);
            this.outputTexBox.Multiline = true;
            this.outputTexBox.Name = "outputTexBox";
            this.outputTexBox.ReadOnly = true;
            this.outputTexBox.Size = new System.Drawing.Size(216, 215);
            this.outputTexBox.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.outputTexBox);
            this.Controls.Add(this.generateCardButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "CardR";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button generateCardButton;
        private System.Windows.Forms.TextBox outputTexBox;
    }
}

