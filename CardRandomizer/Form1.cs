﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardRandomizer
{
    public partial class Form1 : Form
    {
        List<Card> cards;
        Random random;
        public Form1()
        {
            InitializeComponent();
            cards = new List<Card>();
            random = new Random();
        }

        private void generateCardButton_Click(object sender, EventArgs e)
        {
            cards.Clear();

            outputTexBox.Clear();
            outputTexBox.Text += "5 random cards:\r\n";

            for(int i =0; i < 5; i++)
            {
                cards.Add(new Card((SuitsEnum)random.Next(4), (ValuesEnum)random.Next(1, 14)));
                outputTexBox.Text += cards[i] + "\r\n";
            }

            outputTexBox.Text += "\r\nSorted cards:\r\n";

            cards.Sort(new CardComparer_byValue());

            foreach (Card card in cards)
            {
                outputTexBox.Text += card + "\r\n";
            }
        }
    }
}
